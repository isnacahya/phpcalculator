<?php

return [
    -h, --help
    -q, --quite
    -v, --version
        --ansi
        --no-ansi
    -n. --no-interaction
        --env
    -v|vv|vvv, --verbose
    add
    
];
